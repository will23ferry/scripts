#!/bin/bash

if (( $EUID != 0 )); then
    echo "********** WARNING: script not executed - script must be run as sudo **********"
    exit
fi

# add repo, and install vagrant
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install vagrant
