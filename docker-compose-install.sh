#! /bin/bash

# script must be run as sudo 
if (( $EUID != 0 )); then
    echo "********** WARNING: script not executed - script must be run as sudo **********"
    exit
fi



# download docker compose
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# make binary executable - adjust location if needed or uncomment below to create symlink
chmod +x /usr/local/bin/docker-compose

# alternatively, create a symlink
# ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

