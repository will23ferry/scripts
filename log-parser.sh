#!/bin/bash

# this script will parse an Apache log file for 404's, and the IP addresses making the requests.
# it then prints them out, as unique IP addresses with the number of requests per IP
# awk index, and this general format can be used to easily parse through, and extract data from 
# various types of logs

echo "input path to log file"
read INPUT

# verify input filetype is log or txt, additional log types can be added by appending || [[ $INPUT != *.{fileExtension} ]]
if [[ $INPUT != *.log ]]; then

        echo "not a valid file type"
        exit
fi

while read line;
do
        # uncomment for verbose output including path, and status response
        # grep 404 | awk '{print $5, $17, $18, $26, $27 > "verbose-output.txt" }'
        grep 404 | awk '{ print $5 > "output.txt" }'

done < "$INPUT"

filtered=./output.txt

while read line;
do
        # get unique ip addresses, and their number of bad requests
        uniq -c | awk '{ print "IP " $2" : num bad reqs "$1 > "unique-IPs.txt" }'

done < "$filtered"

# uncomment to remove intermediate file, depending on output needs
# rm  $filtered

exit 0
