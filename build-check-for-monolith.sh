#!/usr/bin/env bash

# this is a script to assist in building a monolith NodeJS (React/Express) application backend, and frontend
# the front end will only build if there are changes detected in the client directory
# the front end files are loaded into a bucket, to access via a CDN on GCP (copied from prev commit if no changes, build uploaded if changes exist)

# copy commit_sha.txt from cdn bucket
gsutil cp gs://{BucketName}/cdn/commit_sha.txt ./

# copy build_history.txt from cdn bucket
gsutil cp gs://{BucketName}/cdn/build_history.txt ./

date_time=$(TZ=":America/Denver" date +'%R/%m/%d/%Y')

echo -e "${CI_COMMIT_SHA} ${date_time}\n" >> ./build_history.txt

gsutil cp ./build_history.txt gs://{BucketName}/cdn/build_history.txt

# set PREV_COMMIT_SHA to the contents of the txt file
PREV_COMMIT_SHA=$(<./commit_sha.txt)

echo "previous commit sha: " $PREV_COMMIT_SHA
echo "ci commit sha: " $CI_COMMIT_SHA

if git diff $PREV_COMMIT_SHA $CI_COMMIT_SHA --name-only | grep client/; then

    echo "changes exist in client"

    # Replace the homepage property with the CDN build SHA
    sed -i -e "s/\"homepage\": \".\/\"/\"homepage\": \"\/cdn\/${CI_COMMIT_SHA}\"/g" client/package.json

    npm run client:build

    gsutil -m rsync -r -d client/build gs://{BucketName}/cdn/${CI_COMMIT_SHA}

    mkdir -p api/public && cp -r client/build/* api/public

    echo $CI_COMMIT_SHA > ./commit_sha.txt

    gsutil cp ./commit_sha.txt gs://{BucketName}/cdn/commit_sha.txt
    
# case when no front end changes found
else
    echo "no changes"

    mkdir client/build

    gsutil -m rsync -r -d gs://{BucketName}/cdn/${PREV_COMMIT_SHA} client/build

    gsutil -m rsync -r -d client/build gs://{BucketName}/cdn/${CI_COMMIT_SHA}

    mkdir -p api/public && cp -r client/build/* api/public

    echo $CI_COMMIT_SHA > ./commit_sha.txt

    gsutil cp ./commit_sha.txt gs://{BucketName}/cdn/commit_sha.txt

    # Rename the index.html to index.ejs
    # cp -R client/build/index.html api/public/index.ejs
    # # Replace src or href with the CDN_URL prefix
    # sed -i -e "s/src=\"\//src=\"<%= CDN_URL; %>\//g" api/public/index.ejs
    # sed -i -e "s/href=\"\//href=\"<%= CDN_URL; %>\//g" api/public/index.ejs
fi
