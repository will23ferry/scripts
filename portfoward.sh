#!/bin/bash

# this is just a template, to create a easily repeatable script for SSH port forwarding
# to gain access to resources on the SSH host # network (IE, a VPC on a cloud provider, where you have SSH access to a server, and properly
# configured firewall rules etc...)

# SSH_FWDPORT = the port on your local machine you want to use
# REMOTE_HOST = the IP of the remote server you want to forward traffic from
# REMOTE_FWDPORT = the remote port you are forwarding to local
# SSH_USER = your ssh user on the remote server
# SSH_JUMPPROXY = the remote server you are SSHing into, to access the REMOTE_HOST


ssh -N -L {SSH_FWDPORT}:{REMOTE_HOST}:{REMOTE_FWDPORT} {SSH_USER}@{SSH_JUMPPROXY}
