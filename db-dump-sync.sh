#!/bin/bash

# this script downloads a pgdump from cloud storage, and syncs it to a local PostgreSQL docker container

echo '********** authenticating to cloud storage **********'
docker-compose run db ./scripts/db-dump-sync.sh
docker-compose up -d db

echo '********** syncing local db to latest prod pg_dump **********'
docker-compose run -e PGPASSWORD=postgres db psql -h db -U postgres -d {dbName} -f backup/latest_prod_db.sql 

docker-compose run -e PGPASSWORD=postgres db psql -h db -U postgres -d {dbName} -f ../sql/set-sequence.sql

docker-compose run --rm api npm run db-utils migrate

docker-compose run db rm -rf backup/latest_prod_db.sql

# docker-compose stop db

echo '********** pg_dump db-sync complete **********'
