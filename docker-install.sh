#!/bin/bash

# script to install docker
# curl script onto machine (Using Ansible, Terraform, etc) and install docker
if (( $EUID != 0 )); then
    echo "********** WARNING: script not executed - script must be run as sudo **********"
    exit
fi

# uninstall old versions of docker or docker-engine
apt-get remove docker docker-engine docker.io containerd runc

apt-get update

apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# add Docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# set up stable repo
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


# Update the apt package index, and install the latest version of Docker Engine and containerd
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io

# ***** uncomment to add docker group to sudo group if specific user is not going to be created *****
groupadd docker
usermod -aG docker $USER
newgrp docker # activate group changes


# ***** uncomment this section out to fix the issue of an automatically created ~/.docker dir (if using non root to run script) *****
# sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
# sudo chmod g+rwx "$HOME/.docker" -R

# ***** uncomment to configure docker to start on boot (only linux distros using systemctl) *****
systemctl enable docker.service.
systemctl enable containerd.service

